export function fight(firstFighter, secondFighter) {
  // return winner
  if (whoseFirstTurn() == 2) {
      [firstFighter, secondFighter] = [secondFighter, firstFighter]
  } 
  let { health : firstFighterHealth } = firstFighter;
  let { health : secondFighterHealth } = secondFighter;
  let winner;
  while(true){
    secondFighterHealth -= getDamage(firstFighter, secondFighter);
    if (secondFighterHealth <= 0) {
      winner = firstFighter;
      break;
    }
    firstFighterHealth -= getDamage(secondFighter, firstFighter);
    if (firstFighterHealth <= 0) {
      winner = secondFighter;
      break;
    }
  }
  return winner;
}

export function getDamage(attacker, enemy) {
  // damage = hit - block
  // return damage
  let damage = getHitPower(attacker) - getBlockPower(enemy);
  damage = (damage > 0) ? damage : 0;
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  const { attack } = fighter;
  const criticalHitChance = getRandomBetween();
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  const dodgeChance = getRandomBetween();
  return defense * dodgeChance;
}

function getRandomBetween(min = 1, max = 2) {
  return Math.random() * (max - min) + min;
}
function whoseFirstTurn() {
  return Math.floor(Math.random() * 2) + 1;
}