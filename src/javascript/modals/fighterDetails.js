import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense,  health, source} = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const imageElement = createElement({ tagName: 'span', className: 'fighter-image-preview' });
  
  // show fighter name, attack, defense, health, image

  nameElement.innerText = `Name: ${name} \n`;
  fighterDetails.append(nameElement);

  attackElement.innerText = `Attack: ${attack} \n`;
  fighterDetails.append(attackElement);

  defenseElement.innerText = `Defense: ${defense} \n`;
  fighterDetails.append(defenseElement);

  healthElement.innerText = `Health: ${health} \n`;
  fighterDetails.append(healthElement);

  imageElement.innerHTML=`<img src=\"${source}\" width=\"25%\" height=\"25%\">`
  fighterDetails.append(imageElement);

  return fighterDetails;
}
