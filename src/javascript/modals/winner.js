import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter) {
  const title = 'Winner';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
  // show winner name and image
}

function createWinnerDetails(fighter) {
  const { name, source} = fighter;

  const winnerDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'winner-name' });
  const imageElement = createElement({ tagName: 'span', className: 'winner-image-preview' });
  
  nameElement.innerText = `Name: ${name} \n`;
  winnerDetails.append(nameElement);

  imageElement.innerHTML=`<img src=\"${source}\" width=\"25%\" height=\"25%\">`;
  winnerDetails.append(imageElement);

  return winnerDetails;
}